Teamwork Tutorial Repository
===

Instructions
--

Follow the steps outlined below.

### Clone the repo

1. Clone this repository to your local machine. by going to the address https://bitbucket.org/2016s2wd/version-control-teamwork and clicking the clone button.

###Add a New HTML File

1. Create a new HTML file with the filename team-YOURNAME.html, where YOURNAME is your given name, or initials. For example, Adrian could use: team-adrian.html
1. Add a small amount of HTML with your first name and the date (and time) that the file was created.

### Commit your changes

Save the file and commit it to the repository.

### Push your changes to the repository

Push the changes you have made to the remote repository.

You may find that when you attempt to push your changes it will tell you that there are changes you are missing. If so, pull these into your repository.

Check back on this repository in a few days to see if the other students have added their files. If so, pull them to your local copy.

### Change someone's HTML

Select one of the other students, and edit their file to include some text that says: "Edited by **YOURNAME**", where **YOURNAME** is again your given name.

Save, commit and push.
